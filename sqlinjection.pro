#-------------------------------------------------
#
# Project created by QtCreator 2015-09-14T17:21:30
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sqlinjection
TEMPLATE = app
DESTDIR = $$PWD

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
